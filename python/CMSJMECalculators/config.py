from dataclasses import dataclass, field, fields
from typing import ClassVar, List, Tuple


@dataclass
class JetMETVariationsConfigBase:
    jec: List[str] = field(default_factory=list)
    jesUncertainties: List[Tuple[str, str]] = field(default_factory=list)
    ptResolution: str = ""
    ptResolutionSF: str = ""
    splitJER: bool = False
    useGenMatch: bool = True
    genMatchDR: float = 0.2
    genMatchDPt: float = 3.

    @property
    def jerParams(self):
        return ["ptResolution", "ptResolutionSF",
                "splitJER", "useGenMatch", "genMatchDR", "genMatchDPt"]

    @property
    def params(self):
        pass

    def toCppStr(self, param):
        if isinstance(param, str):
            return f'"{param}"'
        elif isinstance(param, bool):
            return ("true" if param else "false")
        elif isinstance(param, float) or isinstance(param, int):
            return str(param)
        elif hasattr(param, "__iter__"):
            return f'{{{", ".join(self.toCppStr(p) for p in param)}}}'
        else:
            raise ValueError(f"Parameter of unsupported type: {param!r}")

    def toArg(self, value, destType):
        from cppyy import gbl
        if destType in (str, int, float, bool):
            return value
        elif destType == List[Tuple[str, str]]:
            vc = gbl.std.vector["std::pair<std::string,std::string>"]()
            for elm in value:
                vc.push_back(gbl.std.make_pair(*elm))
            return vc
        elif destType == List[str]:
            vc = gbl.std.vector["std::string"]()
            for elm in value:
                vc.push_back(elm)
            return vc
        elif destType == List[float]:
            vc = gbl.std.vector["double"]()
            for elm in value:
                vc.push_back(elm)
            return vc
        elif destType == Tuple[float, float, float, float, float, float]:
            arr = gbl.std.array["double,6"]()
            for i, v in enumerate(value):
                arr[i] = v
            return arr
        else:
            raise RuntimeError(f"Cannot deal with type {destType!r} for value {value}")

    @property
    def cppConstruct(self):
        return (f"{self.calcClass}::create"
                f"({', '.join(self.toCppStr(getattr(self, p)) for p in self.params)})")

    def create(self):
        fields_by_name = {f.name: f for f in fields(self)}
        from cppyy import gbl
        return getattr(gbl, self.calcClass).create(*(
            self.toArg(getattr(self, p), fields_by_name[p].type)
            for p in self.params
        ))


@dataclass
class JetVariations(JetMETVariationsConfigBase):
    calcClass: ClassVar[str] = "JetVariationsCalculator"
    addHEM2018Issue: bool = False

    @property
    def params(self):
        return ["jec", "jesUncertainties", "addHEM2018Issue"] + self.jerParams


@dataclass
class FatJetVariations(JetVariations):
    calcClass: ClassVar[str] = "FatJetVariationsCalculator"
    jms: List[float] = field(default_factory=list)
    gms: List[float] = field(default_factory=list)
    jmr: List[float] = field(default_factory=list)
    gmr: List[float] = field(default_factory=list)
    puppiGenFormula: str = ""
    puppi_reco_cen: Tuple[float, float, float, float, float, float] = (0., 0., 0., 0., 0., 0.)
    puppi_reco_for: Tuple[float, float, float, float, float, float] = (0., 0., 0., 0., 0., 0.)
    puppi_resol_cen: Tuple[float, float, float, float, float, float] = (0., 0., 0., 0., 0., 0.)
    puppi_resol_for: Tuple[float, float, float, float, float, float] = (0., 0., 0., 0., 0., 0.)

    @property
    def params(self):
        return (["jec", "jesUncertainties", "addHEM2018Issue"] + self.jerParams
                + ["jms", "gms", "jmr", "gmr",
                   "puppiGenFormula", "puppi_reco_cen", "puppi_reco_for",
                   "puppi_resol_cen", "puppi_resol_for"])


@dataclass
class METVariationsConfigBase(JetMETVariationsConfigBase):
    l1Jec: List[str] = field(default_factory=list)
    unclusteredEnergyThreshold: float = 15.
    isT1SmearedMET: bool = False


@dataclass
class METVariations(METVariationsConfigBase):
    calcClass: ClassVar[str] = "Type1METVariationsCalculator"
    addHEM2018Issue: bool = False

    @property
    def params(self):
        return ["jec", "l1Jec", "unclusteredEnergyThreshold",
                "jesUncertainties", "addHEM2018Issue", "isT1SmearedMET"] + self.jerParams


@dataclass
class FixEE2017METVariations(METVariationsConfigBase):
    calcClass: ClassVar[str] = "FixEE2017Type1METVariationsCalculator"
    jecProd: List[str] = field(default_factory=list)
    l1JecProd: List[str] = field(default_factory=list)

    @property
    def params(self):
        return ["jec", "l1Jec", "unclusteredEnergyThreshold",
                "jecProd", "l1JecProd", "jesUncertainties",
                "isT1SmearedMET"] + self.jerParams
